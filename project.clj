(defproject ring-test "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [ring/ring-core "1.8.0"]
                 [ring/ring-devel "1.8.0"]
                 [ring/ring-jetty-adapter "1.8.0"]
                 [compojure "1.6.1"]
                 [ring/ring-defaults "0.3.2"]
                 [hiccup "1.0.5"]
                 [metosin/compojure-api "2.0.0-SNAPSHOT"]
                 [metosin/ring-http-response "0.9.1"]
                 [prismatic/schema "1.1.12"]
                 [org.clojure/clojurescript "1.10.597"]
                 [reagent "0.9.1"]]

  :plugins [[lein-ring "0.12.5"]
            [lein-cljsbuild "1.1.7"]
            [lein-figwheel "0.5.14"]]

  :hooks [leiningen.cljsbuild]

  :main ^:skip-aot ring-test.core
  :target-path "target/%s"
  :resource-paths ["resources"]
  :ring {:handler ring-test.handler/app
         :auto-refresh? true}

  :source-paths ["src/clj"]

  :profiles {:uberjar {:aot :all}
             :dev {:cljsbuild
                   {:builds {:client
                             {:figwheel {:on-jsload "ring-test.core/start"}
                              :compiler {:optimizations :none}}}}}}

  :figwheel {:css-dirs ["resources/public/css"]
             :ring-handler ring-test.handler/app
             :server-port 3000}

  :cljsbuild {:builds {:client
                       {:source-paths ["src/cljs"]
                        :compiler {:asset-path "js"
                                   :main "ring-test.core"
                                   :output-to "resources/public/js/main.js"
                                   :output-dir "resources/public/js"}}}})

