(ns ring-test.core
  (:require [ring.adapter.jetty :refer [run-jetty]]
            [ring-test.handler :refer [app]])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Starting server...")
  (run-jetty app
             {:port (Integer/valueOf (or (System/getenv "port")
                                         "3000"))}))

