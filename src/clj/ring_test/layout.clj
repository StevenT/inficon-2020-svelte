(ns ring-test.layout
  (:use [hiccup.page :only (html5 include-css include-js)])
  (:require [ring-test.views :as views]))

(defn application [title & content]
  (html5 {:lang "en"}
         [:head
          [:title title]
          (include-css "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css")
          [:body
           (views/nav)
           [:div.container content]
           [:div#app]
           (include-js "/js/main.js")
           [:script "ring_test.core.start()"]]]))
