(ns ring-test.test)


(defn dummy-stribution
  "Return a meaningless distribution"
  [n]
  (map (partial + n)
       (range (int (* (/ (float (* 2 n))
                         n)
                      10)))))

