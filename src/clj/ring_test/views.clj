(ns ring-test.views
  (:require [hiccup.core :refer [html]]))

(defn nav []
  [:nav.navbar.navbar-expand-sm.bg-dark
   [:ul.navbar-nav
    [:li.nav-item
     [:a.nav-link {:href "/"} "Home"]]
    [:li.nav-item
     [:a.nav-link {:href "/about"} "About"]]
    [:li.nav-item
     [:a.nav-link {:href "/bla"} "Bla"]]
    [:li.nav-item
     [:a.nav-link {:href "/api"} "API"]]
    [:li.nav-item
     [:a.nav-link {:href "/swagger"} "Swagger"]]]])

(defn root []
  [:h1 "Home page"])

(defn about []
  [:h1 "About page"])

(defn not-found []
  [:div
   [:h1 "Not found page"]
   [:p "There's no requested page. "]
   [:a {:class "btn btn-primary"
        :href "/"} "Take me to Home"]])

