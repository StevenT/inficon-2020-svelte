(ns ring-test.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.api.sweet :as sweet]
            [ring.util.http-response :refer [ok]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults api-defaults]]
            [ring-test.views :as views]
            [ring-test.layout :as layout]
            [ring-test.api :as api]
            [schema.core :as s]))

(s/defschema Root
  {:message s/Str})

(def api-routes
  (sweet/api
   {:api {:invalid-routes-fn (constantly nil)}
    :swagger {:ui "/swagger"
              :spec "/swagger.json"
              :data {:info {:title "CustomService"}
                     :tags [{:name "my api"}]}}}

   (sweet/context "/api" []
                  :tags ["main routes"]

                  (sweet/GET "/" []
                       :return Root
                       :summary "Rolls a six sided die"
                       (ok (api/root)))

                  (route/not-found
                       (ok (api/not-found))))))

(defroutes app-routes
  (GET "/" [] (layout/application "Home" (views/root)))
  (GET "/about" [] (layout/application "About" (views/about)))
  (route/not-found (layout/application "Page not found" (views/not-found))))

(def app
  (routes (-> api-routes
              (wrap-defaults api-defaults))
          (-> app-routes
              (wrap-defaults site-defaults))))

