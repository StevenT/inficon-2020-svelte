(ns ring-test.core
  (:require  [reagent.core :as r]))

(enable-console-print!)

(defn page []
  [:div
   [:h1 "Hello from Reagent"]])

(defn ^:export start []
  (js/console.debug "Starting reagent...")
  (r/render [page]
            (js/document.getElementById "app")))

